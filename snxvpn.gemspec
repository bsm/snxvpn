# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4.0'

  s.name        = 'snxvpn'
  s.summary     = 'SNX VPN connection helper'
  s.description = ''
  s.version     = '0.1.2'
  s.license     = 'MIT'

  s.authors     = ['Dimitrij Denissenko']
  s.email       = 'dimitrij@blacksquaremedia.com'
  s.homepage    = 'https://bitbucket.org/bsm/snxvpn'

  s.files         = Dir['bin/**/*'] + Dir['lib/**/*']
  s.executables   = ['snxvpn']
  s.bindir        = 'bin'
  s.require_paths = ["lib"]

  s.add_development_dependency 'rake'
end
