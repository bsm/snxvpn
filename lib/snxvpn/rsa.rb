module Snxvpn
  class RSA < OpenSSL::PKey::RSA

    def self.parse(body)
      mod = body.match(/var *modulus *\= *\'(\w+)\'/).to_a[1]
      exp = body.match(/var *exponent *\= *\'(\w+)\'/).to_a[1]
      new(mod, exp) if mod && exp
    end

    def initialize(mod, exp)
      pubkey = create_encoded_pubkey_from([mod].pack("H*"), [exp].pack("H*"))
      hexkey = [pubkey].pack("H*")
      pemkey =  "-----BEGIN RSA PUBLIC KEY-----\n#{Base64.encode64 hexkey}-----END RSA PUBLIC KEY-----"
      super(pemkey)
    end

    def hex_encrypt(s)
      public_encrypt(s).reverse.unpack("H*").first
    end

    private

    def create_encoded_pubkey_from(rawmod, rawexp)
      modulus_hex = prepad_signed(rawmod.unpack('H*').first)
      exponent_hex = prepad_signed(rawexp.unpack('H*').first)

      modlen = modulus_hex.length / 2
      explen = exponent_hex.length / 2
      encoded_modlen = encode_length_hex(modlen)
      encoded_explen = encode_length_hex(explen)

      full_hex_len = modlen + explen + encoded_modlen.length/2 + encoded_explen.length/2 + 2
      encoded_hex_len = encode_length_hex(full_hex_len)

      "30#{encoded_hex_len}02#{encoded_modlen}#{modulus_hex}02#{encoded_explen}#{exponent_hex}"
    end

    def prepad_signed(hex)
      msb = hex[0]
      (msb < '0' || msb > '7') ? "00#{hex}" : hex
    end

    def encode_length_hex(der_len) # encode ASN.1 DER length field
      return int_to_hex(der_len) if der_len <= 127

      hex = int_to_hex(der_len)
      size = 128 + hex.length/2
      "#{int_to_hex(size)}#{hex}"
    end

    def int_to_hex(num)
      hex = num.to_s(16)
      hex.length.odd? ? "0#{hex}" : hex
    end

  end
end
