module Snxvpn

  class ExtInfo < Hash

    def initialize(body)
      update Hash[body.scan(/Extender.(\w+) *= *"(.*?)" *;/)]
    end

    def payload
      host_ip = IPAddr.new(IPSocket.getaddress(self['host_name']))
      snxinf  = [
        "\x13\x11\x00\x00".encode('binary'),  # 4-byte magic
        [976].pack('L<'),                     # 4-byte data length
        [host_ip.to_i].pack('L<'),            # 4-byte host IP
        self['host_name'].encode('binary').ljust(64, "\0"), # 64 bytes
        [self['port'].to_i].pack('L<'),     # 4-byte port
        ''.encode('binary').ljust(6, "\0"),   # 6-bytes blank
        self['server_cn'].encode('binary').ljust(256, "\0"), # 256 bytes
        self['user_name'].encode('binary').ljust(256, "\0"), # 256 bytes
        self['password'].encode('binary').ljust(128, "\0"),  # 128 bytes
        self['server_fingerprint'].encode('binary').ljust(256, "\0"), # 256 bytes
        "\x01\x00".encode('binary'), # 2 bytes
      ].join
    end

  end

end
