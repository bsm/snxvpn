module Snxvpn
  class Config < Hash
    DEFAULTS = {
      'realm'      => 'ssl_vpn',
      'login_type' => 'Standard',
      'username'   => 'anonymous',
      'password'   => 'secret',
      'snx_path'   => 'snx',
      'entry_path' => '/SNX/Portal/Main',
    }.freeze

    def initialize(path, profile = nil)
      stat = File.stat(path)

      raise ArgumentError, "#{path} is not owned by the current user" unless stat.owned?
      raise ArgumentError, "#{path} mode must be 600" if stat.world_readable? || stat.world_writable?

      raw = YAML.load_file(path)
      raise ArgumentError, "#{path} contains invalid config" unless raw.is_a?(Hash)

      update DEFAULTS
      if profile
        raise ArgumentError, "Unable to find configuration for profile '#{profile}'" unless raw.key?(profile)
        update raw[profile]
      elsif raw.size == 1
        update raw.values.first
      else
        raise ArgumentError, 'No profile selected'
      end
    end

    def [](key)
      super(key.to_s)
    end

  end
end
